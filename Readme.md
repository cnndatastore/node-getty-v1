# node-getty-v1 #
Node.js module with basic functionality for Getty v1 API

# How to use #
	
	var gettyv1 = require('getty-v1');
	
	var creds = {
		SystemId: YOUR_SYSTEM_ID,
		SystemPassword: YOUR_SYSTEM_PASSWORD,
		UserName: USERNAME,
		UserPassword: PASSWORD
	};
	
	var gettyConn = gettyv1(creds);
	
	gettyConn.getImageDetails(['464704630', '464686108'], function(error, response) {
        console.log(error, response);
    });
	
	gettyConn.download({'464704630': "image.jpg"}, function(error, response) {
		console.log(error, response);
	});