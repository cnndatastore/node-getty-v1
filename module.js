// PARTIAL API DOCUMENTATION: https://github.com/gettyimages/gettyimages-api/blob/master/v2/README.md

module.exports = function(apiCreds) {
	return new GettyConn(apiCreds);
};

var Utils = require("./Utils");
var request = require('request');

function GettyConn(apiCreds) {
	this.apiCreds = apiCreds;
}

GettyConn.prototype.download = function(imageRequests, callback) {
	// imageRequests in the form of {imageId: localFile, imageId2: localFile2, ...}
	var self = this;
	self.getDownloadToken(Object.keys(imageRequests), function(error, tokens) {
		self.downloadRequest(tokens, function(error, downloadRequestResponse) {
			var response= {};

			Utils.objectForEach(downloadRequestResponse, function(downloadRequest, imageId) {
				Utils.fileRequest(downloadRequest['UrlAttachment'], imageRequests[imageId], function(error, status) {
					if(error)
						response[imageId] = error;
					else
						response[imageId] = status;
					if(Object.keys(response).length == Object.keys(imageRequests).length)
						callback(null, response);
				});
			});
		});
	});
};

GettyConn.prototype.getSession = function(callback) {
	var self = this;
	var options = {
		uri: "https://connect.gettyimages.com/v1/session/CreateSession",
		method: "POST",
		json: {
			RequestHeader: {
				Token: null,
				CoordinationId: null
			},
			CreateSessionRequestBody: self.apiCreds
		}
	};

	request(options, function(error, response, json) {
		if(error) {
			if(typeof callback === "function")
				callback(error, null);
		}
		else if(!Utils.objectIsset(json, ['CreateSessionResult', 'Token']) || !Utils.objectIsset(json, ['CreateSessionResult', 'SecureToken']) || !Utils.objectIsset(json, ['CreateSessionResult', 'TokenDurationMinutes'])) {
			if(typeof callback === "function")
				callback("invalid_api_response", null);
		}
		else {
			var tokens = Utils.objectGet(json, ['CreateSessionResult']);

			var minutesValid = Utils.objectGet(json, ['CreateSessionResult', 'TokenDurationMinutes']);
			var dateNow = new Date(); // may make this a system-wide function
			var newDateObj = new Date(dateNow.getTime() + (minutesValid - 5) * 60000); // subtract 5 mins to keep some pad

			tokens['validUntil'] = newDateObj;

			if(typeof callback === "function")
				callback(null, tokens);
			self.session = tokens;
		}
	});
};

GettyConn.prototype.searchForImages = function(imageId, callback) {
	this.getSession(function(error, session) {
		if(error)
			callback(error, null);
		else {
			var options = {
				uri: "https://connect.gettyimages.com/v1/search/SearchForImages",
				method: "POST",
				json: {
					RequestHeader: {
						Token: session['SecureToken'],
						CoordinationId: null
					},
					'SearchForImages2RequestBody': {
						Query: {
							"SearchPhrase":"nimoy"
						},
						ResultOptions: {
							'EditorialSortOrder': 'MostPopular'
						},
						Filter: {
							'ImageFamilies': ['editorial'],
						},
						ResultOptions: {
							ItemCount: 10
						}
					}


				}
			};

			request(options, function(error, response, json) {
				console.log(json);
			});
		}
	});
};

GettyConn.prototype.getImageDetails = function(imageIds, callback) {
	this.getSession(function(error, session) {
		if(error)
			callback(error, null);
		else {
			var options = {
				uri: "https://connect.gettyimages.com/v1/search/GetImageDetails",
				method: "POST",
				json: {
					RequestHeader: {
						Token: session['SecureToken'],
						CoordinationId: null
					},
					'GetImageDetailsRequestBody': {
						ImageIds: imageIds
					}
				}
			};

			request(options, function(error, response, json) {

				try {
					var images = {};
					Utils.objectGet(json, ['GetImageDetailsResult', 'Images']).forEach(function(image) {
						images[image['ImageId'].toString()] = image;
					});

					if(typeof imageIds === "string")
						imageIds = [imageIds];

					var imageErrors = null;
					imageIds.forEach(function(imageId) {
						if(typeof images[imageId] !== "object") {
							if(imageErrors == null)
								imageErrors = {};
							imageErrors[imageId.toString()] = "not_found";
						}
					});

					callback(imageErrors, images);

				}
				catch(error) {
					callback(error, null);
				}
			});
		}
	});
};

GettyConn.prototype.getDownloadToken = function(imageIds, callback) {
	this.getSession(function(error, session) {
		if(error)
			callback(error, null);
		else {
			var imageRequest = [];
			imageIds.forEach(function(imageId) {
				imageRequest.push({'ImageId': imageId})
			});

			var options = {
				uri: "https://connect.gettyimages.com/v1/download/GetLargestImageDownloadAuthorizations",
				method: "POST",
				json: {
					RequestHeader: {
						Token: session['SecureToken'],
						CoordinationId: null
					},
					'GetLargestImageDownloadAuthorizationsRequestBody': {
						'Images': imageRequest
					}
				}
			};

			request(options, function(error, response, json) {
				if(error)
					callback(error, null);
				else if(!Utils.objectIsset(json, ['GetLargestImageDownloadAuthorizationsResult', 'Images']))
					callback("invalid_api_response", null);
				else {
					var downloadTokens = {};
					json['GetLargestImageDownloadAuthorizationsResult']['Images'].forEach(function(downloadTokenResponse) {
						downloadTokens[downloadTokenResponse['ImageId']] = downloadTokenResponse['Authorizations'][0];
					});
					callback(null, downloadTokens);
				}
			});
		}
	});
};

GettyConn.prototype.downloadRequest = function(downloadInfos, callback) {
	this.getSession(function(error, session) {
		if(error)
			callback(error, null);
		else {
			var imageRequest = [];
			Utils.objectForEach(downloadInfos, function(downloadInfo) {
				console.log(downloadInfo);
				imageRequest.push({'DownloadToken': downloadInfo['DownloadToken']})
			});

			var options = {
				uri: "https://connect.gettyimages.com/v1/download/CreateDownloadRequest",
				method: "POST",
				json: {
					RequestHeader: {
						Token: session['SecureToken'],
						CoordinationId: null
					},
					'CreateDownloadRequestBody': {
						'DownloadItems': imageRequest
					}
				}
			};

			request(options, function(error, response, json) {
				if(error)
					callback(error, null);
				else if(!Utils.objectIsset(json, ['CreateDownloadRequestResult', 'DownloadUrls']))
					callback("invalid_api_response", null);
				else {
					var downloadRequests = {};
					json['CreateDownloadRequestResult']['DownloadUrls'].forEach(function(downloadResponse) {
						downloadRequests[downloadResponse['ImageId']] = downloadResponse;
					});
					callback(null, downloadRequests);
				}
			});
		}
	});
};
